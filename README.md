# Arduino Estación Meteorológica v1

Como conversábamos con algunos colegas docentes respecto a los proyectos con fines educativos, les comparto el proyecto de una "Estación Meteorológica" muy básica, utilizando los sensores DHT11 (temperatura y humedad), BMP180 (presión atmosférica y cálculo de altura SNM) y un display LCD para mostrar la información recabada.

Podría agregársele, un sensor de humedad del suelo y tal vez un sensor de intensidad de UV como el ML8511, y hasta un adaptador para guardar en una SD la información de un determinado período (habrá que sumarle un reloj de tiempo real)

Como siempre, espero que les guste y les sea de utilidad.

Traté de documentar el código lo más posible.

Las librerías utilizadas son:

DHT https://learn.adafruit.com/dht/downloads
LCD https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library
BMP180 https://github.com/adafruit/Adafruit_BMP085_Unified
