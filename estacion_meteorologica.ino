
//Cargar librerías del sensor de humedad DHT
#include <DHT_U.h>
#include <DHT.h>

//Cargar librerías para LCD
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>

//Cargar librerías para el sensor de Presión
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP085_U.h>

//Conexiones
//Vin = 3.3 volts
//GND = GND
//SDA = UNO pin A4 - MEGA pin 20
//SCL = UNO pin A5 - MEGA pin 21

//Las librerías pueden descargarse de:
// DHT https://learn.adafruit.com/dht/downloads
// LCD https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library
// BMP180 https://github.com/adafruit/Adafruit_BMP085_Unified



// Parametrización del LCD

//Define variables 

#define I2C_ADDR          0x27        //Define I2C Address where the PCF8574A is
#define BACKLIGHT_PIN      3
#define En_pin             2
#define Rw_pin             1
#define Rs_pin             0
#define D4_pin             4
#define D5_pin             5
#define D6_pin             6
#define D7_pin             7


//Inicializar el LCD
LiquidCrystal_I2C      lcd(I2C_ADDR, En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);



//Inicializar el sensor DHT
#define DHTTYPE DHT11   // DHT 11
const int pinSensorDHT = 5;

DHT sensorDHT(pinSensorDHT, DHTTYPE); //5 es el pin que leerá el sensor


//Creamos una instancia del sensor BMP180
Adafruit_BMP085_Unified bmp180 = Adafruit_BMP085_Unified(10085);



void setup()
 {
    //Establecer si se trata de un LCD 16x2 o 20x4 (caracteres x líneas) 
    lcd.begin (20,4);
    
    //Encender la iluminación de fondo
    lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
    lcd.setBacklight(HIGH);

    //Posicionar el cursor en columa, fila
    lcd.setCursor(1,0);
    
    //Muestro el mensaje
    lcd.print("EST. METEOROLOGICA");


    //Inicializo el sensor DHT (Temperatura y Humedad)
    sensorDHT.begin();

    //Inicializo el sensor BMP180 (Presión, Temperatura y Altura)
    bmp180.begin();
}


//Función auxiliar
void prepararLCD()
{
    int tiempoEspera = 2000;
    
    //Espera entre lecturas
    delay(tiempoEspera); 
    //Limpio la pantalla
    lcd.clear();
    //Posicionar el cursor en columa, fila
    lcd.setCursor(1,0);
    //Muestro el mensaje
    lcd.print("EST. METEOROLOGICA");
    
}


 
void loop(){



    String mensaje_temp = "";
    String mensaje_hum = "";
    String mensaje_sensor = "";
    String mensaje_altura = "";
    String mensaje_presion = "";
    
    prepararLCD();

    //----------------- SENSOR DHT -----------------

    float temperatura = sensorDHT.readTemperature();
    float humedad = sensorDHT.readHumidity();

    //Si obtengo datos, los muestro... en caso contrario, aviso del error
    if(isnan(temperatura) || isnan(humedad))
    {
      lcd.setCursor(1,2);
      mensaje_temp = "Error de lectura";
    }
    else
    {
      mensaje_temp = "Temperat.: "+String(temperatura)+"*C";
      mensaje_hum = "Humedad:"+String(humedad)+"%";
    }

    lcd.setCursor(1,1);
    lcd.print("Sensor DHT");
    lcd.setCursor(1,2);
    lcd.print(mensaje_temp);
    lcd.setCursor(1,3);
    lcd.print(mensaje_hum);


    prepararLCD();

    //----------------- SENSOR BMP -----------------

    sensors_event_t evento;
    bmp180.getEvent(&evento);

    if(!evento.pressure)
    {
      lcd.setCursor(1,2);
      mensaje_temp = "Error de lectura";
    }
    else
    {
      mensaje_presion = "Presion: "+String(evento.pressure)+"hPa";
      
      //Tomando valor de Río Gallegos, según https://www.tutiempo.net/rio-gallegos.html
      float seaLevelPressure = 1014.00;      
      mensaje_altura = "Altura SNM: "+String(bmp180.pressureToAltitude(seaLevelPressure, evento.pressure))+"m";
    }

    lcd.setCursor(1,1);
    lcd.print("Sensor BMP180");
    lcd.setCursor(1,2);
    lcd.print(mensaje_presion);
    lcd.setCursor(1,3);
    lcd.print(mensaje_altura);
    

  }
 
